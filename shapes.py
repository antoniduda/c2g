from math import tan, cos, radians


class Parallelogram:
    def __init__(self, x, y, w, h, theta=30):
        self.theta = theta
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        t = self.h * tan(radians(theta))
        self.polygon = [x - t, y + h, x + w, y + h, x + w + t, y, x, y]


class Rhombus:
    def __init__(self, x, y, size=100.0, theta=90):
        self.theta = theta
        self.size = size
        self.x = x
        self.y = y

        t = cos(radians(theta))

        self.polygon = [
            x - size,
            y,
            x,
            y + size * (t - 1),
            x + size,
            y,
            x,
            y + size * (1 - t),
        ]
