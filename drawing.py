from tkinter import *
from tkinter.font import Font
from math import *
from shapes import Parallelogram, Rhombus


class Graph:
    def __init__(self):
        self.window = Tk()
        self.window.title("C2G")
        self.window.geometry("600x480+10+10")
        self.canvas = Canvas(self.window, width=600, height=480)
        self.canvas.pack()

    def start(self):
        self.window.mainloop()

    def add_operation_box(self, x, y, text, size=11):
        font = Font(family="Consolas", size=size)
        w = font.measure(text)
        self.canvas.create_rectangle(
            x - 8, y - 12, x + w + 8, y + size * 4 / 3 + 12, width=1.5, outline="#AAA"
        )
        self.canvas.create_rectangle(x - 8, y - 12, x + w + 8, y + size * 4 / 3 + 12)
        self.canvas.create_text(x + w / 2, y + size * 4 / 3 / 2, text=text, font=font)

    def add_io_box(self, x, y, text, size=11):
        font = Font(family="Consolas", size=size)
        w = font.measure(text)
        self.canvas.create_polygon(
            Parallelogram(x - 8, y - 12, w + 16, size * 4 / 3 + 24).polygon,
            fill="",
            width=1.5,
            outline="#AAA",
        )
        self.canvas.create_polygon(
            Parallelogram(x - 8, y - 12, w + 16, size * 4 / 3 + 24).polygon,
            outline="black",
            fill="",
        )
        self.canvas.create_text(x + w / 2, y + size * 4 / 3 / 2, text=text, font=font)

    def add_start_stop_box(self, x, y, text, size=11):
        font = Font(family="Consolas", size=size)
        w = font.measure(text)
        self.canvas.create_oval(
            x - 8, y - 12, x + w + 8, y + size * 4 / 3 + 12, width=1.5, outline="#AAA"
        )
        self.canvas.create_oval(x - 8, y - 12, x + w + 8, y + size * 4 / 3 + 12)
        self.canvas.create_text(x + w / 2, y + size * 4 / 3 / 2, text=text, font=font)

    def add_if_box(self, x, y, text, size=11):
        font = Font(family="Consolas", size=size)
        w = font.measure(text)
        self.canvas.create_polygon(
            Rhombus(x + w / 2, y + w / 2, w * 1.5).polygon,
            fill="",
            width=1.5,
            outline="#AAA",
        )
        self.canvas.create_polygon(
            Rhombus(x + w / 2, y + w / 2, w * 1.5).polygon,
            outline="black",
            fill="",
        )
        self.canvas.create_text(x + w / 2, y + size * 4 / 3 / 2, text=text, font=font)


g = Graph()
g.add_operation_box(100, 100, "a=10")
g.add_io_box(100, 200, "out: a")
g.add_start_stop_box(100, 300, "START")
g.add_if_box(100, 400, "IF")
g.start()
